package az.ingress.mskafka.consumer;

import az.ingress.mskafka.dto.Student;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service
@Slf4j
@RequiredArgsConstructor
public class KafkaConsumer {

    // @KafkaListener(topics = "demo-kafka", groupId = "myGroup")
    public void consumeMsg(String msg) {
        log.info(format("Consuming the message from demo-kafka topic %s", msg));
    }

    @KafkaListener(topics = "demo-kafka", groupId = "myGroup")
    public void consumeJsonMsg(Student student) {
        log.info(format("Consuming the message from demo-kafka topic %s", student.toString()));
    }
}
