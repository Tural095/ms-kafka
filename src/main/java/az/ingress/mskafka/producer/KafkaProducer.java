package az.ingress.mskafka.producer;

import az.ingress.mskafka.prop.KafkaTopicProperties;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import static java.lang.String.format;

@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaProducer {

    private final KafkaTemplate<String, String> kafkaTemplate;
    private final KafkaTopicProperties kafkaTopicProperties;

    public void sendMessage(String msg) {
        log.info(format("Sending message to demo-kafka topic %s", msg));
        kafkaTemplate.send("demo-kafka", msg);
    }
}
