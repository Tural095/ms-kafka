package az.ingress.mskafka;

import lombok.RequiredArgsConstructor;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


@RequiredArgsConstructor
@SpringBootApplication
public class MsKafkaApplication {

    public static void main(String[] args) {
        SpringApplication.run(MsKafkaApplication.class, args);
    }

}
