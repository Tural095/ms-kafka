package az.ingress.mskafka.controller;

import az.ingress.mskafka.dto.Student;
import az.ingress.mskafka.producer.KafkaJsonProducer;
import az.ingress.mskafka.producer.KafkaProducer;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/messages")
@RequiredArgsConstructor
public class KafkaDemoController {

    private final KafkaProducer kafkaProducer;
    private final KafkaJsonProducer kafkaJsonProducer;

    @PostMapping
    public String sendMessage(@RequestBody String message) {
        kafkaProducer.sendMessage(message);
        return "Message queued successfully";
    }

    @PostMapping("/json")
    public String sendJsonMessage(@RequestBody Student message) {
        kafkaJsonProducer.sendMessage(message);
        return "Message queued successfully as JSON";
    }
}
